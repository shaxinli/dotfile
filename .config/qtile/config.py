# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import os
import subprocess
from typing import List  # noqa: F401
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile import hook

mod = "mod4"
terminal = "alacritty"
myBrowser = "firefox" # My terminal of choice
home = os.path.expanduser('~')

## defining functions to send windows to/from other screen
def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    ##Key([mod], "space", lazy.layout.next(),
        ##desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r",  lazy.spawn("rofi -show drun -show-icons"), desc="Launch rofi"),

    Key([mod, "shift"], "period", lazy.function(window_to_previous_screen)),
    Key([mod, "shift"], "comma", lazy.function(window_to_next_screen)),
    Key([mod], "t", lazy.function(switch_screens)),

    # Toggle layouts

    Key([mod, "shift"], "space", lazy.window.toggle_floating()),

]



groups = []

   # Allocate layouts and labels
group_names = ["1", "2", "3", "4", "5", "6", "7", "8",]
group_labels = ["", "", "", "", "", "", "", "",]
group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])


## defining colours(DT's color)
colors = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#4f76c7", "#4f76c7"], # color for the 'even widgets'
          ["#e1acff", "#e1acff"], # window name
          ["#ecbbfb", "#ecbbfb"]] # backbround for inactive screens
# defining clours(old teck bloke's color)
colors1= [["#2E3440", "#2E3440"], # color 0
          ["#2E3440", "#2E3440"], # color 1
          ["#c0c5ce", "#c0c5ce"], # color 2
          ["#fba922", "#fba922"], # color 3
          ["#3384d0", "#3384d0"], # color 4
          ["#f3f4f5", "#f3f4f5"], # color 5
          ["#cd1f3f", "#cd1f3f"], # color 6
          ["#62FF00", "#62FF00"], # color 7
          ["#6790eb", "#6790eb"], # color 8
          ["#a9a9a9", "#a9a9a9"]] # color 9

#
## def of base parameter for the theme to use later
def init_layout_theme():
    return {"margin":5,
            "border_width":2,
            "border_focus": "#5e81ac",
            "border_normal": "#4c566a"
            }
## calling the function to initalise the layout_theme
layout_theme = init_layout_theme()


## defining a list of layouts, also includes commected layout
layouts = [
    # layout.Columns(border_focus_stack='#d75f5f'),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Stack(stacks=2, **layout_theme),
    layout.Max(**layout_theme)
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]


## defining widgets
widget_defaults = dict(
    font='Fira Code Nerd Font',
    fontsize=14,
    padding=2,
    background=colors1[1]
    )

extension_defaults = widget_defaults.copy()

## creating a default widget list
def init_widgets_list():
    widget_default_list=[widget.GroupBox(active = colors1[5],
                                inactive = colors1[6],
                                highlight_method = "line",
                                highlight_color = colors1[4],
                                this_screen_border = colors1[4],
                                other_screen_border = colors1[6],
                                foreground = colors1[2],
                                background = colors1[1]
                                ),
                         widget.Prompt(),
                         widget.TaskList(max_title_width=200,
                                foreground = colors1[2],
                                background = colors1[1]),


                #widget.CPU(format = '{load_percent}% ',
                           #foreground = colors1[6],
                           #background = colors1[1]),
                #widget.Net(format = '⬇{down} ⬆{up}',
                           #padding = 5,
                           #foreground = colors1[4],
                           #background = colors1[1]),
                widget.PulseVolume(
                    font="Fira Code Nerd Font",
                    fontsize=12,
                    foreground="4285f4",
                    emoji=False,
                    limit_max_volume=True
                ),
                widget.Systray(foreground = colors1[2],
                               background = colors1[1]),
                widget.CurrentLayoutIcon(scale = 0.5,
                                         foreground = colors1[2],
                                         background = colors1[1]),
                #widget.TextBox(
                        #font="Fira Code Nerd Font",
                        #text = " 🌡",
                        #padding = 2,
                        #foreground = colors1[2],
                        #background = colors1[1],
                        #fontsize = 12
                         #),

                #widget.ThermalSensor(
               #         font="Fira Code Nerd Font",
                #        fontsize = 12,
                 #       fmt = '{} ',
                  #      foreground = colors1[2],
                   #     background = colors1[1],
                    #    threshold = 90,
                     #   padding = 5
                      #  ),

                #widget.OpenWeather(location='london',

                 #                  foreground = colors1[2],
                 #                  background = colors1[1]),
                #widget.TextBox(
                #       text = '',
                #       background = colors1[1],
                #       foreground = colors1[2],
                #       padding = 0,
                #       fontsize = 40),
                #widget.Clock(format='%Y-%m-%d %a %I:%M %p',
                 #            foreground = colors[2],
                  #           background = colors[1]),
                widget.Clock(
                       foreground = colors1[1],
                       background = colors1[2],
                       fontsize=12,
                       format = "⏱ %H:%M "
                       ),]
    return widget_default_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()[0:3]
    return widgets_screen2


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26, margin = [5,8,-3,8])),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26, margin = [5,8,-3,8]))]


screens = init_screens()

main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])
    
# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "focus"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True





# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
