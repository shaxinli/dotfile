function dotfile --wraps='/usr/bin/git --git-dir=/home/bobo/.cfg/ --work-tree=/home/bobo' --description 'alias dotfile=/usr/bin/git --git-dir=/home/bobo/.cfg/ --work-tree=/home/bobo'
  /usr/bin/git --git-dir=/home/bobo/.cfg/ --work-tree=/home/bobo $argv; 
end
